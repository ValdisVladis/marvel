//
//  MarvelCategoryCollectionViewCell.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//
//
protocol CollectionViewDelegate {
    func populateCollectionViewCell(with object: MarvelCategories)
}

import UIKit

class MarvelCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var coverForItem: UIImageView!
    
    static let reusableIdentifier = "MarvelCategoryCollectionViewCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        contentView.backgroundColor = UIColor.white.withAlphaComponent(1.0)
        contentView.alpha = 0.8
        contentView.layer.cornerRadius = 10
        contentView.layer.borderWidth = 2
        contentView.layer.borderColor = UIColor.gray.cgColor
        
    }
    override class func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "MarvelCategoryCollectionViewCell", bundle: nil)
    }
    
}
extension MarvelCategoryCollectionViewCell: CollectionViewDelegate {
    func populateCollectionViewCell(with object: MarvelCategories) {
        categoryTitle.text = "\(object.categoryTitle)"
        coverForItem.image = object.contentViewBackgroundImage

    }
}

