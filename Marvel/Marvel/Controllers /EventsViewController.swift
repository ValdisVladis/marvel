//
//  EventsViewController.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//
import Moya
import UIKit

class EventsViewController: UIViewController {
    
    let provider = MoyaProvider<Marvel>()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
        EventsViewControllerManager.shared.getEventsData()

        // Do any additional setup after loading the view.
    }
 
}
