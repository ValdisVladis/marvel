//
//  SeriesViewController.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//
import Moya
import UIKit

class SeriesViewController: UIViewController {
    
    let provider = MoyaProvider<Marvel>()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPink
        SeriesViewControllerManager.shared.getSeriesData()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
