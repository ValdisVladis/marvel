//
//  CharactersViewController.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//
import Moya
import UIKit

class CharactersViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        CharactersViewControllerManager.shared.getCharactersData()

    }

}
