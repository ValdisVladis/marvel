//
//  LaunchViewController.swift
//  Marvel
//
//  Created by Владислав on 7/2/21.
//
import Kingfisher
import UIKit

class LaunchViewController: UIViewController {
    private var viewModel: MainViewModelProtocol!
    private var launchViewControllerView: LaunchControllerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainViewModel()
        createLaunchViewControllerView()
        updateLaunchViewControllerView()
        createGestureRecognizerToPopOverNextController()
    }
    func createLaunchViewControllerView() {
        launchViewControllerView = LaunchControllerView()
        launchViewControllerView.frame = view.bounds
        view.addSubview(launchViewControllerView)
    }
    private func updateLaunchViewControllerView() {
        viewModel.updateViewData = { [weak self] viewData in
            guard let self = self else {return}
            self.launchViewControllerView.viewData = viewData
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.startFetchingData()
        
    }
}
extension UIViewController {
    @objc func popToNext(swipe: UISwipeGestureRecognizer) {
        _ = self.instantiateViewController(identifier: "CategoryViewController", title: nil, transitionStyle: .flipHorizontal, presentationStyle: .fullScreen)
    }

}
extension LaunchViewController {
    func createGestureRecognizerToPopOverNextController() {
        let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(popToNext(swipe:)))
        gestureRecognizer.direction = .left
        self.view.addGestureRecognizer(gestureRecognizer)
    }
}
   
