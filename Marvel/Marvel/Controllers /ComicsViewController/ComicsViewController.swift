//
//  ComicsViewController.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//
import Kingfisher
import UIKit

class ComicsViewController: UIViewController {
    private var comicsViewModel: ComicsControllerViewModelProtocol!
    private var comicsControllerView: ComicsControllerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        comicsViewModel = ComicsViewModel()
        createComicControllerView()
        updateComicsViewData()
        
        
    }
    
    private func createComicControllerView() {
        comicsControllerView = ComicsControllerView()
        comicsControllerView.frame = CGRect(x: 100, y: 100, width: 300, height: 150)
        view.addSubview(comicsControllerView)
    }
    
    private func updateComicsViewData() {
        comicsViewModel.updateComicsViewData = {[weak self] comicsViewData in
            guard let self = self else {return}
            self.comicsControllerView.comicViewData = comicsViewData
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        comicsViewModel.startFetchingData()
    }
    
}
