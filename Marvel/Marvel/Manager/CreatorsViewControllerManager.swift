//
//  CreatorsViewControllerManager.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//

import Foundation
import UIKit
import Moya

class CreatorsViewControllerManager {
    static let shared = CreatorsViewControllerManager()
    private init() {}
    
    func getCreatorsData() {
        let provider = MoyaProvider<Marvel>()
        provider.request(.creators) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("Our creator's data is \(try response.mapJSON())")
                }catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print("The data was not received because of \(error.errorDescription ?? "")")
            }
        }
    }
}
