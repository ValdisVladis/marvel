//
//  EventsViewControllerManager .swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//

import Foundation
import UIKit
import Moya

class  EventsViewControllerManager {
    static let shared = EventsViewControllerManager()
    private init() {}
    
    func getEventsData() {
        let provider = MoyaProvider<Marvel>()
        provider.request(.events) { [weak self ](result) in
            guard let self = self else  { return }
            switch result {
            case .success(let events):
                do {
                    print("Our event's data is \(try events.mapJSON())")
                }catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
    }
}
