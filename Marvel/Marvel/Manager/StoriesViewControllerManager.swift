//
//  StoriesViewControllerManager.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//

import Foundation
import UIKit
import Moya

class StoriesViewControllerManager {
    static let shared = StoriesViewControllerManager()
    private init() {}
    
    func getStoriesData() {
        let provider = MoyaProvider<Marvel>()
        provider.request(.stories) { (result) in
            switch result {
            case .success(let stories):
                do {
                    print("Our story's data is \(try stories.mapJSON())")
                }catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
    }
}
