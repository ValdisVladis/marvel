//
//  CategoryViewControllerManager.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//
import Moya
import Foundation
import UIKit

class CategoryViewControllerManager {
    
    static let shared = CategoryViewControllerManager()
    private init() {}
    
    func getMarvelCategoryModel() -> [MarvelCategories]  {
        let categories = [MarvelCategories(categoryTitle: "Comics", contentViewBackgroundImage: UIImage(named: "coverImage")),
                          MarvelCategories(categoryTitle: "Characters", contentViewBackgroundImage: UIImage(named: "ironman")),
                          MarvelCategories(categoryTitle: "Creators", contentViewBackgroundImage: UIImage(named: "stanleepng")),
                          MarvelCategories(categoryTitle: "Events", contentViewBackgroundImage: UIImage(named: "marvelevents")),
                          MarvelCategories(categoryTitle: "Series", contentViewBackgroundImage: UIImage(named: "marvelseries")),
                          MarvelCategories(categoryTitle: "Stories", contentViewBackgroundImage: UIImage(named: "marvelstories"))]
        return categories
    }
    
    func setupCollectionViewFlowLayout(collectionView: UICollectionView) -> CGSize {
        let countCells = 2
        let offset: CGFloat = 3.0
        let frameCV = collectionView.frame
        let widthCell = frameCV.width / CGFloat(countCells)
        let heightCell = widthCell
        let spacing = CGFloat(countCells + 1) * offset / CGFloat(countCells)
        return CGSize(width: widthCell - spacing, height: heightCell - (offset*2))
    }
    
    func returnCollectionViewCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MarvelCategoryCollectionViewCell.reusableIdentifier, for: indexPath) as? MarvelCategoryCollectionViewCell else {return UICollectionViewCell()}
        collectionViewCell.populateCollectionViewCell(with: CategoryViewControllerManager.shared.getMarvelCategoryModel()[indexPath.item])
        
        return collectionViewCell
    }
    
    
}
