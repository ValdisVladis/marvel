//
//  SeriesViewControllerManager.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//

import Foundation
import UIKit
import Moya

class SeriesViewControllerManager {
    static let shared = SeriesViewControllerManager()
    private init() {}
    
    func getSeriesData() {
        let provider = MoyaProvider<Marvel>()
        provider.request(.series) { (result) in
            switch result {
            case .success(let series):
                do {
                    print("Our series data is \(try series.mapJSON())")
                }catch {
                    print("error is \(error.localizedDescription)")
                }
            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
    }
}
