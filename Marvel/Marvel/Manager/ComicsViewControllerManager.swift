//
//  ComicsViewControllerManager.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//

import Foundation
import UIKit
import Moya

class ComicsViewControllerManager {
    
    static let shared = ComicsViewControllerManager()
    private init() {}
    var completion: (([Comics]) ->Void)?
    
    func getComicsData() {
        let provider = MoyaProvider<Marvel>()
        provider.request(.comics) { (result) in
            switch result {
            case .success(let response):
                do {
                    let comicsData = try response.map(MarvelResponse<Comics>.self).data.results
                    self.completion?(comicsData)
                    print("Our Marvel data is \(comicsData)")
                }catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
    }
    
}
