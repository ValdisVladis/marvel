//
//  CharactersViewControllerManager.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//
import Moya
import Foundation
import UIKit

class CharactersViewControllerManager {
    static let shared = CharactersViewControllerManager()
    
    private init() {}
    
    func getCharactersData() {
        let provider = MoyaProvider<Marvel>()
        provider.request(.characters) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("Our character data is \(try response.mapJSON())")
                }catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
        
    }
}
