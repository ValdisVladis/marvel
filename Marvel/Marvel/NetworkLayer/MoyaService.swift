//
//  MoyaService.swift
//  Marvel
//
//  Created by Владислав on 6/27/21.
//

import Foundation
import Moya

public enum Marvel {
    static private let publicKey = "e75e31e6018444f9d20eaa94717184cc"
    static private let privateKey = "6eafbc116c3c1639879587bf9c256128ad33c7e8"
    
    case comics
    case characters
    case creators
    case events
    case series
    case stories
}

extension Marvel: TargetType {
    public var baseURL: URL {
        return URL(string: "https://gateway.marvel.com/v1/public")!
    }
    
    public var path: String {
        switch self {
        
        case .comics:
            return "/comics"
        case .characters:
            return "/characters"
        case .creators:
            return "/creators"
        case .events:
            return "/events"
        case .series:
            return "/series"
        case .stories:
            return "/stories"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        
        case .comics:
            return .get
        case .characters:
            return .get
        case .creators:
            return .get
        case .events:
            return .get
        case .series:
            return .get
        case .stories:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        let ts = "\(Date().timeIntervalSince1970)"
        // 1
        let hash = (ts + Marvel.privateKey + Marvel.publicKey).md5
        
        // 2
        let authParams = ["apikey": Marvel.publicKey, "ts": ts, "hash": hash]
        
        switch self {
        case .comics:
            // 3
            return .requestParameters(
                parameters: [
                    "format": "comic",
                    "formatType": "comic",
                    "orderBy": "-onsaleDate",
                    "dateDescriptor": "lastWeek",
                    "limit": 50] + authParams,
                encoding: URLEncoding.default)
            
        case .characters:
            return .requestParameters(parameters: [
                                        "orderBy": "modified",
                                        "limit": 50] + authParams,
                                      encoding: URLEncoding.default)
        case .creators:
            return .requestParameters(parameters: [
                                        "orderBy": "modified",
                                        "limit": 50] + authParams,
                                      encoding: URLEncoding.default)
        case .events:
            return .requestParameters(parameters: [
                                        "orderBy": "-modified",
                                        "limit": 50],
                                      encoding: URLEncoding.default)
        case .series:
            return .requestParameters(parameters: [
                                        "orderBy": "-modified",
                                        "limit": 50],
                                      encoding: URLEncoding.default)
        case .stories:
            return .requestParameters(parameters: [
                                        "orderBy": "-modified",
                                        "limit": 50],
                                      encoding: URLEncoding.default)
        }
        
    }
    
    public var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
    
    
}
