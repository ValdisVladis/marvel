//
//  CodableResponse.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//

import Foundation

struct MarvelResponse<T: Codable>: Codable {
  let data: MarvelResults<T>
}

struct MarvelResults<T: Codable>: Codable {
  let results: [T]
}
