//
//  MarvelCategories.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//
import UIKit
import Foundation

class MarvelCategories {
    let categoryTitle: String
    var contentViewBackgroundImage: UIImage
    
    init(categoryTitle: String, contentViewBackgroundImage: UIImage?) {
        self.categoryTitle = categoryTitle
        self.contentViewBackgroundImage = contentViewBackgroundImage ?? UIImage()
    }
}
