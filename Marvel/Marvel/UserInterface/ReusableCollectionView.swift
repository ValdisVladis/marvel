//
//  ReusableCollectionView.swift
//  Marvel
//
//  Created by Владислав on 6/30/21.
//

import Foundation
import UIKit

extension CategoryViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let categoryName = CategoryViewControllerManager.shared.getMarvelCategoryModel()[indexPath.row].categoryTitle
        switch indexPath.item {
        case 0:
            _ = self.instantiateViewController(identifier: "ComicsViewController", title: categoryName, transitionStyle: .crossDissolve, presentationStyle: .formSheet)
        case 1:
            _ = self.instantiateViewController(identifier: "CharactersViewController", title: categoryName, transitionStyle: .partialCurl, presentationStyle: .overFullScreen)
        case 2:
            _ = self.instantiateViewController(identifier: "CreatorsViewController", title: categoryName, transitionStyle: .coverVertical, presentationStyle: .formSheet)
        case 3:
            _ = self.instantiateViewController(identifier: "EventsViewController", title: categoryName, transitionStyle: .partialCurl, presentationStyle: .pageSheet)
        case 4:
            let seriesViewController = self.instantiateViewController(identifier: "SeriesViewController", title: categoryName, transitionStyle: .crossDissolve, presentationStyle: .currentContext)
            seriesViewController.title = categoryName
        case 5:
            _ = self.instantiateViewController(identifier: "StoriesViewController", title: categoryName, transitionStyle: .crossDissolve, presentationStyle: .pageSheet)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    }
    
    
    
}
extension CategoryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CategoryViewControllerManager.shared.getMarvelCategoryModel().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return CategoryViewControllerManager.shared.returnCollectionViewCell(collectionView: collectionView, indexPath: indexPath)
       
    }
    
    
}
extension CategoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CategoryViewControllerManager.shared.setupCollectionViewFlowLayout(collectionView: collectionView)
        
    }
    
}
