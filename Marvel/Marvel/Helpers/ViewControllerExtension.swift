//
//  ViewControllerExtension.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//

import Foundation
import UIKit



extension UIViewController {
    public func instantiateViewController(identifier: String, title: String?, transitionStyle: UIModalTransitionStyle, presentationStyle: UIModalPresentationStyle) -> UIViewController {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: identifier) {
            viewController.title = title
            viewController.modalTransitionStyle = transitionStyle
            viewController.modalPresentationStyle = presentationStyle
            navigationController?.pushViewController(viewController, animated: true)
            return viewController
        }else{
            return UIViewController()
        }
        
    }
    
}
