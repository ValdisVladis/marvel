//
//  GlobalFunctions.swift
//  Marvel
//
//  Created by Владислав on 6/28/21.
//

import Foundation
import UIKit

public func getImageFromAssets(name: String) -> UIImage {
    guard let image = UIImage(named: name) else {return UIImage()}
    return image
}

