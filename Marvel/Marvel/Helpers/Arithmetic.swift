//
//  Arithmetic.swift
//  Marvel
//
//  Created by Владислав on 7/1/21.
//

import Foundation

precedencegroup ExponentiationPrecedence {
    associativity: right
    higherThan: MultiplicationPrecedence
}
infix operator ** : ExponentiationPrecedence


protocol Exponentiation {
    static func **(base: Self, exp: Self) -> Self
}

protocol Arithmetic {
    
    static func +(lhs: Self, rhs: Self) -> Self
    static func -(lhs: Self, rhs: Self) -> Self
    static func *(lhs: Self, rhs: Self) -> Self
    static func /(lhs: Self, rhs: Self) -> Self
    
    
}

class Calculator {
    
    static let shared = Calculator()
    private init() {}
    
    func add<T: Arithmetic>(left: T, right: T) -> T {
        return left + right
    }
    
    func subtract<T: Arithmetic>(left: T, right: T) -> T {
        return left - right
    }
    
    func multiply<T: Arithmetic>(left: T, right: T) -> T {
        return left * right
    }
    
    func divide<T: Arithmetic>(left: T, right: T) -> T {
        return left / right
    }
    
    func exponentiate<T: Exponentiation>(base: T, exp: T) -> T {
        return base ** exp
    }
    
}




extension Double: Exponentiation {
    static func ** (base: Double, exp: Double) -> Double {
        return pow(base, exp)
    }
}

extension Int: Arithmetic {
    
}

extension Double: Arithmetic {
    
    
}

