//
//  ComicsControllerViewModelProtocol.swift
//  Marvel
//
//  Created by Владислав on 7/5/21.
//
import Moya
import Foundation
import UIKit

protocol ComicsControllerViewModelProtocol {
    var updateComicsViewData: ((ComicsViewData)->Void)? {get set}
    func startFetchingData()
}

final class ComicsViewModel: ComicsControllerViewModelProtocol {
    public var updateComicsViewData: ((ComicsViewData) -> Void)?
    
    init() {
        self.updateComicsViewData?(.initialize)
    }
    
    public func startFetchingData() {
       let provider = MoyaProvider<Marvel>()
        provider.request(.comics) { (result) in
            switch result {
            
            case .success(let response):
                do {
                    let comics = try response.map(MarvelResponse<Comics>.self).data.results
                    self.updateComicsViewData?(.success(comics))
                }catch {
                    print(error.localizedDescription)
                }
            case .failure(let failure):
                print("Error has been given", failure.errorDescription)
            }
        }
    
    }
    
    
}
