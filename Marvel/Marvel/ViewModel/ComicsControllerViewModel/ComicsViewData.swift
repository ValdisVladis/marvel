//
//  ComicsViewData.swift
//  Marvel
//
//  Created by Владислав on 7/5/21.
//

import Foundation

enum ComicsViewData {
    case initialize
    case loading
    case success([Comics])
    case failure
}
