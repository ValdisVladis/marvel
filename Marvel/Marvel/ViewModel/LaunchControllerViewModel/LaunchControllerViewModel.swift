//
//  LaunchControllerViewModel.swift
//  Marvel
//
//  Created by Владислав on 7/4/21.
//

import Foundation
import UIKit

enum ViewData {
    case initialize
    case loading
    case ready(RequestData)
    case failure
    
    struct RequestData {
        let urls: [URL]?
    }
}

