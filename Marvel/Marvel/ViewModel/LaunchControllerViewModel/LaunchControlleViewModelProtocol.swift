//
//  LaunchControlleViewModelProtocol.swift
//  Marvel
//
//  Created by Владислав on 7/4/21.
//

import UIKit
import Foundation

protocol MainViewModelProtocol {
    var updateViewData: ((ViewData) -> ())? { get set }
    func startFetchingData()
}

final class MainViewModel: MainViewModelProtocol {
    public var updateViewData: ((ViewData) -> ())?
    
    init() {
        updateViewData?(.initialize)
    }
    
    public func startFetchingData() {
        
        updateViewData?(.ready(ViewData.RequestData(urls: [
                                                        URL(string: "https://i.pinimg.com/564x/27/78/0f/27780fa56eb4c65850b7b466bb096dfa.jpg")!,
                                                        URL(string: "https://pbs.twimg.com/media/E5BVsz8UcAM6mmM.jpg")!,
                                                        URL(string: "https://www.fortressofsolitude.co.za/wp-content/uploads/2021/04/Marvel-Studios-Loki-Trailer-Poster.jpg")!,
                                                        URL(string: "https://i.guim.co.uk/img/media/aff9c61a560ec4dc99744b32fa3b9b56a0021f22/0_0_1215_1800/master/1215.jpg?width=700&quality=85&auto=format&fit=max&s=15bfe9642cd73a4559eb4956f7d4b1bb")!,
                                                        URL(string: "https://i.redd.it/ni9qj4bfkay61.jpg")!,
                                                        URL(string: "https://static.wikia.nocookie.net/marveldatabase/images/0/04/Black_Cat_Vol_1_3_Bring_on_the_Bad_Guys_Unknown_Comic_Books_Exclusive_Virgin_Variant.jpg/revision/latest?cb=20190808090414")!])))
        
        
    }
}

