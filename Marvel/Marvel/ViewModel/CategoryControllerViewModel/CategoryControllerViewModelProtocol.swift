//
//  CategoryControllerViewModelProtocol.swift
//  Marvel
//
//  Created by Владислав on 7/4/21.
//

import Foundation
import UIKit

protocol CategoryControllerViewModelProtocol {
    var updateCategoryViewData: ((CategoryViewData)->Void)? {get set}
    func updateUserInterface()
}

final class CategoryViewModel: CategoryControllerViewModelProtocol {
    public var updateCategoryViewData: ((CategoryViewData) -> Void)?
    
    init() {
        updateCategoryViewData?(.initialize)
    }
    
    public func updateUserInterface() {
        updateCategoryViewData?(.viewDidAppear([MarvelCategories(categoryTitle: "Comics", contentViewBackgroundImage: UIImage(named: "coverImage")),
                                                MarvelCategories(categoryTitle: "Characters", contentViewBackgroundImage: nil),
                                                MarvelCategories(categoryTitle: "Creators", contentViewBackgroundImage: nil),
                                                MarvelCategories(categoryTitle: "Events", contentViewBackgroundImage: nil),
                                                MarvelCategories(categoryTitle: "Series", contentViewBackgroundImage: nil),
                                                MarvelCategories(categoryTitle: "Stories", contentViewBackgroundImage: nil)]))
    }
    
    
}

