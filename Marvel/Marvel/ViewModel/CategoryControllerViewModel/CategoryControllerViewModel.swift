//
//  CategoryControllerViewModel.swift
//  Marvel
//
//  Created by Владислав on 7/4/21.
//

import Foundation
import UIKit

enum CategoryViewData {
    case initialize
    case load
    case viewDidAppear([MarvelCategories])
}

