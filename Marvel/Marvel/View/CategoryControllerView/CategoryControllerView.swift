//
//  CategoryControllerView.swift
//  Marvel
//
//  Created by Владислав on 7/4/21.
//

import UIKit


//class CategoryControllerView: UIView {
//    var categoryData: [MarvelCategories] = []
//    lazy var categoryCollectionView = setupCategoryCollectionView()
//    var categoryViewData: CategoryViewData = .initialize {
//        didSet {
//            categoryCollectionView.dataSource = self
//            categoryCollectionView.delegate = self
//
//            setNeedsLayout()
//        }
//    }
//    
//    override func layoutSubviews()  {
//        super.layoutSubviews()
//        switch categoryViewData {
//        case .initialize:
//           print("Category view has been initialized")
//        case .load:
//            print("Category view has loaded")
//        case .viewDidAppear(let category):
//            self.categoryData = category
//            addSubview(categoryCollectionView)
//            self.categoryCollectionView.reloadData()
////            self.categoryCollectionView.reloadData()
//
//        }
//    }
//    
//    
//
//}
//extension CategoryControllerView {
//    func setupCategoryCollectionView() -> UICollectionView {
//        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height), collectionViewLayout: setupLayoutForCollectionView())
//        collectionView.register(MarvelCategoryCollectionViewCell.nib(), forCellWithReuseIdentifier: MarvelCategoryCollectionViewCell.reusableIdentifier)
////        collectionView.delegate = self
////        collectionView.dataSource = self
//        collectionView.isUserInteractionEnabled = true
//        collectionView.isPagingEnabled = true
//        collectionView.backgroundView = setupBackgroundViewForCategoryCollectionView()
//        collectionView.frame = self.bounds
//        collectionView.addSubview(setupBackgroundViewForCategoryCollectionView())
//        return collectionView
//        
//    }
//    
//    func setupLayoutForCollectionView() -> UICollectionViewFlowLayout {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical
//        return layout
//    }
//    
//    func setupBackgroundViewForCategoryCollectionView() -> UIImageView {
//        let backgroundImage = UIImageView(image: UIImage(named: "backgroundImageForCollectionView"))
//        backgroundImage.frame = self.setupCategoryCollectionView().bounds
//        return backgroundImage
//    }
//    
//}
//extension CategoryControllerView: UICollectionViewDelegate {
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        collectionView.deselectItem(at: indexPath, animated: true)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 20
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
//    }
//    
//}
//extension CategoryControllerView: UICollectionViewDataSource {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return categoryData.count
//      
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        guard let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: MarvelCategoryCollectionViewCell.reusableIdentifier, for: indexPath) as? MarvelCategoryCollectionViewCell else {return UICollectionViewCell()}
//        categoryCell.populateCollectionViewCell(with: categoryData[indexPath.item])
//        return categoryCell
//    }
//}
//extension CategoryControllerView: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        CategoryViewControllerManager.shared.setupCollectionViewFlowLayout(collectionView: collectionView)
//        
//    }
//    
//}
