//
//  ComicsControllerView.swift
//  Marvel
//
//  Created by Владислав on 7/5/21.
//

import UIKit

class ComicsControllerView: UIView {
    
    lazy var comicImage = getComicsImage()
    var comicViewData: ComicsViewData = .initialize {
        didSet {
           setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch comicViewData {
        
        case .initialize:
            print("view has been initialized")
        case .loading:
            print("view is loading")
        case .success(let comics):
            comics.forEach { (comic) in
                self.comicImage.kf.setImage(with: comic.thumbnail.url)
            }
          
        case .failure:
            print("Failure has been received")
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension ComicsControllerView {
    func getComicsImage() -> UIImageView {
        let comicImage = UIImageView()
        comicImage.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
        comicImage.center = self.center
        comicImage.contentMode = .scaleAspectFit
        self.addSubview(comicImage)
        return comicImage
    }
}
