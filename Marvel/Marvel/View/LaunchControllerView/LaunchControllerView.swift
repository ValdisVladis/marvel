//
//  LaunchControllerView.swift
//  Marvel
//
//  Created by Владислав on 7/4/21.
//
import Kingfisher
import UIKit

class LaunchControllerView: UIView {
    lazy var mainMarvelView = setupMainView()
    var viewData: ViewData = .initialize {
        didSet {
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch viewData {
        case .initialize:
            print("View has been initialized")
        case .loading:
            print("data has not been loaded yet")
        case .ready(let ready):
            guard let urls = ready.urls else { return}
            let shuffled = urls.shuffled()
            for eachURL in shuffled {
                DispatchQueue.main.async {
                    self.mainMarvelView.kf.setImage(with: eachURL)
                }
            }
            
        case .failure:
            print("error has been given")
        }
    }
    
}

extension LaunchControllerView {
    func setupMainView() -> UIImageView {
        let mainImageView = UIImageView()
        mainImageView.contentMode = .scaleAspectFill
        mainImageView.frame = self.bounds
        addSubview(mainImageView)
        return mainImageView
    }
    
    
}
